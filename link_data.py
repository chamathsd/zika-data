from unidecode import unidecode
import csv, difflib, sys

csv.field_size_limit(sys.maxsize)

location_map = {}
province_list = []

with open('locations.csv', newline='') as csvread:
  reader = csv.reader(csvread, delimiter=';')
  count = 1

  next(reader)
  for row in reader:
      name_clean = unidecode(row[1])
      alt_names_clean = unidecode(row[2])
      country_name_clean = unidecode(row[6])
      
      names_split = alt_names_clean.split('|')
      names_split += [name_clean]
      for name in names_split:
          if name != "" and -170 < float(row[5]) < -30:
            province_list += [country_name_clean + "-" + name]
            location_map[country_name_clean + "-" + name] = count
      count += 1

print("\nLinking reports with location data...")
with open ('reports.csv', newline='') as csvread:
  with open('reports_linked.csv', 'w', newline='') as csvwrite:
    reader = csv.reader(csvread, delimiter=',')
    writer = csv.writer(csvwrite, delimiter=',')
    writer.writerow(["report_date", "location", "location_type", "location_id", "data_field", "data_field_code", "value", "unit"])
    
    next(reader)
    count = 0
    for row in reader:
      if count % 50 == 0:
          percent = round(count / 240272.0, 4)
          progress = int(round(percent * 60))
          sys.stdout.write("\rProgress: [" + "=" * progress + " " * (60 - progress) + "] " + format(percent * 100, '.2f') + "%")
          sys.stdout.flush()
      count += 1

      match_string = ""
      closest = None

      split_location = row[1].split('-')
      if len(split_location) < 2:
          continue

      match_string = split_location[0] + '-' + split_location[1]
      matches = difflib.get_close_matches(match_string, province_list, 1, 0.45)

      if len(matches) > 0:
        closest = location_map[matches[0]]

      if closest != None:
        writer.writerow([row[0],       row[1],     row[2],          closest,      row[3],        row[4],            row[7],  row[8]])

print("\nCompleted")

