f = open('reports.csv','r')
filedata = f.read()
f.close()

f = open('reports.csv.bak', 'w')
f.write(filedata)
f.close()

newdata = filedata.replace(" 00:00:00","")
newdata = newdata.replace("Zika", "zika")
newdata = newdata.replace("cummulative", "cumulative")
newdata = newdata.replace("pregnt", "pregnant")
newdata = newdata.replace("Pama", "Panama")
newdata = newdata.replace("United_States-Puerto_Rico", "Puerto_Rico-Puerto_Rico")

f = open('reports.csv','w')
f.write(newdata)
f.close()
