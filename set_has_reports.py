import csv, sys

csv.field_size_limit(sys.maxsize)

mapped_locations = set()

with open('reports_linked.csv', newline='') as csvread:
  reader = csv.reader(csvread, delimiter=',')

  next(reader)
  for row in reader:
    mapped_locations.add(row[3])

print("\nMarking locations with existing reports...")
with open ('locations.csv', newline='') as csvread:
  with open('locations_linked.csv', 'w', newline='') as csvwrite:
    reader = csv.reader(csvread, delimiter=';')
    writer = csv.writer(csvwrite, delimiter=';')
    writer.writerow(next(reader) + ['has_reports'])
    
    idx = 1
    for row in reader:
      writer.writerow(row + ["true" if str(idx) in mapped_locations else "false"])
      idx += 1

print("\nCompleted")

