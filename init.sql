-- SQL environment for application
BEGIN;

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;
CREATE EXTENSION IF NOT EXISTS unaccent;

CREATE TABLE location
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    name_alt VARCHAR(255),
    country VARCHAR(255),
    code_hasc VARCHAR(255),
    latitude NUMERIC,
    longitude NUMERIC,
    has_reports BOOLEAN,
    geom GEOMETRY
);

CREATE TABLE report
(
    id SERIAL PRIMARY KEY,
    report_date VARCHAR(255),
    location VARCHAR(255),
    location_type VARCHAR(255),
    location_id INTEGER REFERENCES location(id),
    data_field VARCHAR(255),
    data_field_code VARCHAR(255),
    value INTEGER DEFAULT 0,
    unit VARCHAR(255)
);

ALTER TABLE location OWNER TO zika_user;
ALTER TABLE report OWNER TO zika_user;

COMMIT;

