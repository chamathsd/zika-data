import csv

report_types = set()

with open ('reports.csv', newline='') as csvread:
    reader = csv.reader(csvread, delimiter=',')
    for row in reader:
        report_types.add(row[1])

for elem in sorted(report_types):
    print(elem)

print("\n", len(report_types))
