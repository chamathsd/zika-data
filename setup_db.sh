#!/bin/bash

export PGPASSWORD=$4

# Reset databases
psql -h $1 -p $2 -U $3 -d postgres << EOF
DROP DATABASE IF EXISTS zika;
DROP DATABASE IF EXISTS zika_test;

CREATE DATABASE zika OWNER zika_user ENCODING 'UTF8' TEMPLATE template_postgis;
CREATE DATABASE zika_test OWNER zika_user ENCODING 'UTF8' TEMPLATE template_postgis;
EOF

# Initialize prod and testing databases
psql -h $1 -p $2 -U $3 -d zika -f init.sql
psql -h $1 -p $2 -U $3 -d zika_test -f init.sql

# Populate production db with base data
psql -h $1 -p $2 -U $3 -d zika -c "\COPY location(geom, name, name_alt, code_hasc, latitude, longitude, country, has_reports) FROM STDIN DELIMITER ';' CSV HEADER" < locations_linked.csv

psql -h $1 -p $2 -U $3 -d zika -c "\COPY report(report_date, location, location_type, location_id, data_field, data_field_code, value, unit) FROM STDIN DELIMITER ',' CSV HEADER" < reports_linked.csv

